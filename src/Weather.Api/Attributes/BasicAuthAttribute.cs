using Microsoft.AspNetCore.Mvc;
using Weather.Api.Middlewares;

namespace Weather.Api.Attributes;

public class BasicAuthAttribute
    : TypeFilterAttribute
{
    public BasicAuthAttribute(Type type) 
        : base(type)
    {
    }
    
    public BasicAuthAttribute(string realm = @"My Realm") 
        : base(typeof(BasicAuthFilter))
    {
        Arguments = new object[] { realm };
    }
}