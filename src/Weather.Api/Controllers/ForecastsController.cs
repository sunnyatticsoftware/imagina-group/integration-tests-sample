using Microsoft.AspNetCore.Mvc;
using Weather.Api.Attributes;

namespace Weather.Api.Controllers;

[ApiController]
[BasicAuth]
[Route("api/[controller]")]
public class ForecastsController
    : ControllerBase
{
    private readonly IConfiguration _configuration;

    public ForecastsController(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    
    [HttpGet]
    public IActionResult GetForecast()
    {
        var forecast = _configuration.GetValue<string>("ForecastToday");
        return Ok(forecast);
    }
}