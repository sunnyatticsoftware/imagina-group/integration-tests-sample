using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Weather.Api.Middlewares;

public class BasicAuthFilter
    : IAuthorizationFilter
{
    private readonly string _realm;
    
    public BasicAuthFilter(string realm)
    {
        _realm = realm;
        if (string.IsNullOrWhiteSpace(_realm))
        {
            throw new ArgumentNullException(nameof(realm), @"Please provide a non-empty realm value.");
        }
            
    }
    
    public void OnAuthorization(AuthorizationFilterContext context)
    {
        var isAuthorized = IsAuthorized(context);
        if (!isAuthorized)
        {
            ReturnUnauthorizedResult(context);
        }
    }

    private bool IsAuthorized(AuthorizationFilterContext context)
    {
        var authHeader = context.HttpContext.Request.Headers["Authorization"].ToString();
        
        if (string.IsNullOrWhiteSpace(authHeader))
        {
            return false;
        }

        return true;
    }

    private void ReturnUnauthorizedResult(AuthorizationFilterContext context)
    {
        // Return 401 and a basic authentication challenge (causes browser to show login dialog)
        context.HttpContext.Response.Headers["WWW-Authenticate"] = $"Basic realm=\"{_realm}\"";
        context.Result = new UnauthorizedResult();
    }
}