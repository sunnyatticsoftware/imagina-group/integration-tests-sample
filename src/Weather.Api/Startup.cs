namespace Weather.Api;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        // IoCC
        services.AddControllers();
    }

    public void Configure(IApplicationBuilder app)
    {
        // Middleware

        app.UseRouting();
        app.UseEndpoints(configure =>
        {
            configure.MapControllers();
        });
    }
}