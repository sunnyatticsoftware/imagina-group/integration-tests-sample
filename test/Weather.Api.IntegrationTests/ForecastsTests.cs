using FluentAssertions;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xunit;

namespace Weather.Api.IntegrationTests;

public class ForecastsTests
    : IntegrationTest
{
    [Fact]
    public async Task Should_Return_401_When_Request_Has_No_Authorization_Header()
    {
        var result = await UnauthorizedHttpClient.GetAsync("api/forecasts");
        result.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
    }
    
    [Fact]
    public async Task Should_Return_200_Ok()
    {
        var result = await HttpClient.GetAsync("api/forecasts");
        result.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    
    [Fact]
    public async Task Should_Return_Expected_Forecast_From_Environment_Json_Settings()
    {
        var result = await HttpClient.GetAsync("api/forecasts");
        var payload = await result.Content.ReadAsStringAsync();
        payload.Should().Be("thunderstorm");
    }
}