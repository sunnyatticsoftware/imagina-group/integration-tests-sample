using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;
using System.Net.Http.Headers;
using Weather.Api.IntegrationTests.Extensions;

namespace Weather.Api.IntegrationTests;

public abstract class IntegrationTest
{
    public HttpClient UnauthorizedHttpClient { get; }
    public HttpClient HttpClient { get; }
    
    protected IntegrationTest()
    {
        var server = new TestServer(
            new WebHostBuilder()
                .UseStartup<Startup>()
                .UseCommonConfiguration()
                .UseEnvironment("Test")
                .ConfigureServices(ConfigureTestServices)
            );

        UnauthorizedHttpClient = server.CreateClient();
        HttpClient = server.CreateClient();
        HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "cGF0YXRhOmJ1ZW5h");
    }

    protected virtual void ConfigureTestServices(IServiceCollection services)
    {
        // registros por defecto
    }
}